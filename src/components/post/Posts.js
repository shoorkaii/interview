import React from 'react'
import {ScrollView} from 'react-native'
import PostItem from './PostItem'

class Posts extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<ScrollView>
				{this.props.data.map((item, index) => {
					return <PostItem item={item}/>
				})}
			</ScrollView>
		)
	}
}

export default Posts