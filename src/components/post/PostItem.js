import React from 'react'
import {Image, Text, View} from 'react-native'

class PostItem extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<View style={{width: 100, height: 100}}>
				<View style={{flexDirection: 'row'}}>
					<Image source={require('../../assets/images/Canon_EOS_5D_Mark_III.jpg')}
					       style={{height: 80, width: 100}}
					/>
					<Text style={{textAlign: 'center'}}>'PostItem'</Text>
				</View>
				<View style={{flexDirection: 'row'}}>
					<Image source={require('../../assets/images/Canon_EOS_5D_Mark_III.jpg')}
					       style={{height: 80, width: 100}}
					/>
					<Text style={{textAlign: 'center'}}>'Content'</Text>
				</View>
			</View>
		)
	}
}

export default PostItem