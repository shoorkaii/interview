import React from 'react'
import {Text, View} from 'react-native'

class Home extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<View style={{flex: 1,}}>
				<Text style={{textAlign: 'center'}}>
					Please feel free to ask me any questions.
				</Text>
			</View>
		)
	}
}

export default Home